( function () {
	'use strict';
	var textBox = document.getElementById( 'wpTextbox1' );
	var checkbox = document.getElementById( 'wpTranslateAutoPrepare' );

	$( '#editform' ).submit( function () {
		if ( !checkbox.checked ) {
			return; // disabled
		}

		var text = textBox.value;

		if ( text.includes( "<translate>" ) ) {
			return; // already done manually
		}

		textBox.value = "<languages/>\n<translate>\n" + text.trim() + "\n</translate>";
	} )
}() );
